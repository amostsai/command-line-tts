#-*- coding: utf-8 -*-
#!/usr/bin/python
import optparse, urllib2, sys, csv
def TTS(input_string, input_file, output_file):
    input_string = str(input_string.replace(" ", "%20"))
    input_file = str(input_file)
    output_file = str(output_file)

    def converter(string, filename):
        url_template = "http://translate.google.com/translate_tts?ie=UTF-8&tl=zh&q='{string}'"
        request = urllib2.Request(url_template.format(string=string.replace(" ", "%20")))
        request.add_header('User-agent', 'Mozilla/5.0') 
        opener = urllib2.build_opener()

        f = open("{filename}.mp3".format(filename=filename), "wb")
        f.write(opener.open(request).read())
        f.close()
        print "[success]  convert {string} -> {filename}.mp3".format(string=string, filename=filename)

    if input_file == "":
        converter(input_string, output_file)
    else:
        csv_content = csv.reader(open(input_file,"rb"))
        for string, filename in csv_content:    
            converter(string, filename)


if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option("-s", "--string", dest="string", default= "TTS 文字轉語音", help="string to voice", metavar="STRING")
    parser.add_option("-i", "--input", dest="input", default="", help="csv file name. in csv file, first column is 'string' you want to speak, second column is file name you wan to create. when you use -i, you don't need to use -s and -o", metavar="INPUT")
    parser.add_option("-o", "--output", dest="output", default="string2voice", help="output file name", metavar="OUTPUT")
    (options,args )= parser.parse_args()

    TTS(options.string, options.input, options.output)